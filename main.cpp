#include <ctime>
#include <vector>
#include <sstream>
#include <random>
#include "src/Layers/ConvolutionalLayer.h"
#include "src/Layers/FullyConnectedLayer.h"
#include "src/Layers/ReLuLayer.h"
#include "src/Layers/SoftmaxLayer.h"
#include "src/Layers/PoolLayer.h"
#include "src/DataPoint.h"
#include "src/ConfusionMatrix.h"
#include "src/common.h"

using namespace CNN;

int main(int argc, char *argv[]) {
    std::vector<Layer *> layers;

    srand(time(nullptr));
    std::vector<DataPoint *> data = read("mnist_train.txt");

    auto *cnn_layer1 = new ConvolutionalLayer(1, 5, 5, {INPUT_WIDTH, INPUT_HEIGHT, INPUT_DEPTH});
    auto *relu_layer1 = new ReLuLayer(cnn_layer1->output->size);
    auto *cnn_layer2 = new ConvolutionalLayer(1, 3, 4, relu_layer1->output->size);
    auto *relu_layer2 = new ReLuLayer(cnn_layer2->output->size);
    auto *pool_layer = new PoolLayer(2, 2, relu_layer2->output->size);
    auto *fc1 = new FullyConnectedLayer(pool_layer->output->size, 82);
    auto *relu_layer3 = new ReLuLayer(fc1->output->size);
    auto *fc2 = new FullyConnectedLayer(relu_layer3->output->size, 10);
    auto *softmax_layer = new SoftmaxLayer(fc2->output->size);
    layers.push_back(cnn_layer1);
    layers.push_back(relu_layer1);
    layers.push_back(cnn_layer2);
    layers.push_back(relu_layer2);
    layers.push_back(pool_layer);
    layers.push_back(fc1);
    layers.push_back(relu_layer3);
    layers.push_back(fc2);
    layers.push_back(softmax_layer);

    float error = 0;
    int count = 100;
    float cost_error = 0;
    int costs_step = 50;
    std::vector<float> costs;

    /*std::ifstream file("weights.txt");
    for(auto &layer:layers){
        layer->loadWeights(file);
    }*/

    for (int iteration = 1; iteration <= data.size(); iteration++) {
        DataPoint *input_case = data[iteration - 1];
        float err = train(layers, input_case);

        error += err;
        cost_error += err;

        if (iteration % costs_step == 0) {
            costs.push_back(cost_error / float(costs_step));
            cost_error = 0;
        }

        if (iteration % count == 0) {
            std::cout << "Last " << count << " training data average error: " << error / float(count) << std::endl;
            error = 0;
            std::cout << "Training progress " << iteration << "/" << data.size() << std::endl;
        }
    }

    for (auto &input_case:data) {
        delete input_case;
    }
    data.clear();
    data = read("mnist_test.txt");
    error = 0;

    auto *matrix = new FloatArray(10, 10, 1);
    int test_count = data.size();

    for (int iteration = 1; iteration <= test_count; iteration++) {
        DataPoint *input_case = data[iteration - 1];

        float err = test(layers, input_case);

        int result = layers.back()->output->maxPosition();
        int label = input_case->label;
        auto *out = new FloatArray(*layers.back()->output);
        input_case->output = out;
        matrix->get(result, label)++;

        error += err;

        if (iteration % count == 0) {
            std::cout << "Last " << count << " test data average error: " << error / float(count) << std::endl;
            std::cout << "Training progress " << iteration << "/" << data.size() << std::endl;
            error = 0;
        }
    }

    ConfusionMatrix confusion_matrix(matrix);
    confusion_matrix.computeMetrics();
    std::ofstream metrics_file("metrics.txt");
    confusion_matrix.printMetrics(metrics_file);
    confusion_matrix.saveROCCurves(data);
    metrics_file.close();

    std::ofstream costs_file("costs.txt");
    for (int i = 1; i <= costs.size(); i++) {
        costs_file << i * 25 << ", " << costs[i-1] << std::endl;
    }
    std::ofstream("weights.txt").close();
    for (auto &layer:layers) {
        layer->saveWeights("weights.txt");
    }

    for (auto &data_point:data) {
        delete data_point;
    }

    delete matrix;
    /*for (auto &layer:layers) {
        delete layer;
    }*/
    return 0;
}
