//
// Created by Damir on 28.05.2021.
//

#include "DataPoint.h"
using namespace CNN;

DataPoint::DataPoint(size_array data_size, size_array out_size) {
    data = new FloatArray(data_size.width, data_size.height, data_size.depth);
    target = new FloatArray(out_size.width, out_size.height, out_size.depth);
}

DataPoint::~DataPoint() {
    delete data;
    delete target;
}
