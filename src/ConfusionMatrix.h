#ifndef LAB3_CONFUSIONMATRIX_H
#define LAB3_CONFUSIONMATRIX_H

#include <vector>
#include "FloatArray.h"
#include "DataPoint.h"
#include<bits/stdc++.h>

namespace CNN {
    class ConfusionMatrix {
    public:
        explicit ConfusionMatrix(FloatArray *matrix);

    public:
        FloatArray *matrix;
        size_array size{};
        std::vector<int> fp;
        std::vector<int> fn;
        std::vector<int> tp;
        std::vector<float> precision;
        std::vector<float> recall;
        std::vector<float> f_score;
        std::vector<float> auc;
        std::vector<std::vector<float>> tpr;
        std::vector<std::vector<float>> fpr;
        std::vector<int> positive;
        std::vector<int> negative;
        float accuracy{};

        void computeMetrics();

        void saveROCCurves(std::vector<DataPoint *> &cases);

        void printMetrics(std::ostream &out = std::cout);
    };
}

#endif //LAB3_CONFUSIONMATRIX_H

