#ifndef LAB3_DATAPOINT_H
#define LAB3_DATAPOINT_H


#include "FloatArray.h"

namespace CNN {

    class DataPoint {
    public:
        FloatArray *data;
        FloatArray *target;
        int label;
        FloatArray *output;

        DataPoint(size_array data_size, size_array out_size);

        ~DataPoint();
    };

}


#endif //LAB3_DATAPOINT_H
