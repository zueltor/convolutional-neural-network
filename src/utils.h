#ifndef LAB3_UTILS_H
#define LAB3_UTILS_H

#include "constants.h"

namespace CNN {

    struct Point {
        int x;
        int y;
        int z;
    };

    struct size_array {
        int width;
        int height;
        int depth;
    };

    struct range_array {
        int min_x, min_y;
        int max_x, max_y;
    };

    static float update_weight(float w, float grad, float mult = 1) {
        static float vt = 0;
        vt = GAMMA * vt + LEARNING_RATE * grad * mult;
        w -= vt;
        return w;
    }

}


#endif //LAB3_UTILS_H
