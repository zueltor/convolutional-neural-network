//
// Created by Damir on 28.05.2021.
//

#ifndef LAB3_COMMON_H
#define LAB3_COMMON_H


#include "Layers/Layer.h"
#include "DataPoint.h"
#include <vector>

namespace CNN {

    float train(std::vector<Layer *> &layers, DataPoint *input_case);

    float test(std::vector<Layer *> &layers, DataPoint *input_case);

    std::vector<DataPoint *> read(const std::string &filename);
}


#endif //LAB3_COMMON_H
