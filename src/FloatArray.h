#ifndef LAB3_FLOATARRAY_H
#define LAB3_FLOATARRAY_H


#include <cassert>
#include <iostream>
#include <cmath>
#include <cstring>
#include <cfloat>
#include "utils.h"

namespace CNN {

    class FloatArray {

    public:
        size_array size{};
        float *values = nullptr;

        void print(std::ostream &out = std::cout) const;

        void printMatrix(std::ostream &out = std::cout) const;

        int maxPosition();

        void read(std::istream &in = std::cin) const {
            for (int z = 0; z < size.depth; z++) {
                for (int y = 0; y < size.height; y++) {
                    for (int x = 0; x < size.width; x++) {
                        in >> get(x, y, z);
                    }

                }
            }
        }

        FloatArray(int width, int height, int depth) : values(new float[width * height * depth]),
                                                       size({width, height, depth}) {
            for (int i = 0; i < width * height * depth; i++) {
                values[i] = 0;
            }
        }

        FloatArray(const FloatArray &t);

        static FloatArray *difference(FloatArray *output, FloatArray *target);

        static float crossEntropy(FloatArray *output, FloatArray *target);

        float &get(int x, int y, int z) const;

        float &get(int x, int y) const;

        float &get(int x) const;

        ~FloatArray();

    };

}

#endif //LAB3_FLOATARRAY_H
