//
// Created by Damir on 28.05.2021.
//

#include "common.h"
#include "constants.h"
#include <fstream>
#include <sstream>

namespace CNN {
    float train(std::vector<Layer *> &layers, DataPoint *input_case) {
        for (int i = 0; i < layers.size(); i++) {
            if (i == 0) {
                layers[i]->activate(input_case->data);
            } else {
                layers[i]->activate(layers[i - 1]->output);
            }
        }

        FloatArray *diff_gradient = FloatArray::difference(layers.back()->output,
                                                           input_case->target);

        for (int i = layers.size() - 1; i >= 0; i--) {
            if (i == layers.size() - 1) {
                layers[i]->calculateInputGradients(*diff_gradient);
            } else {
                layers[i]->calculateInputGradients(*layers[i + 1]->input_gradients);
            }
        }
        delete diff_gradient;

        for (auto &layer : layers) {
            layer->updateWeights();
        }

        float err = FloatArray::crossEntropy(layers.back()->output, input_case->target);

        return err;
    }

    float test(std::vector<Layer *> &layers, CNN::DataPoint *input_case) {
        for (int i = 0; i < layers.size(); i++) {
            Layer *layer = layers[i];

            if (i == 0) { layer->activate(input_case->data); }
            else { layer->activate(layers[i - 1]->output); }
        }

        float err = FloatArray::crossEntropy(layers.back()->output, input_case->target);
        return err;
    }

    std::vector<DataPoint *> read(const std::string &filename) {
        std::ifstream file(filename);
        std::string line;
        size_array input_size{INPUT_WIDTH, INPUT_HEIGHT, INPUT_DEPTH};
        size_array output_size{OUTPUT_WIDTH, OUTPUT_HEIGHT, OUTPUT_DEPTH};
        std::vector < DataPoint * > cases;


        while (getline(file, line)) {
            std::stringstream linestream(line);
            int label;
            linestream >> label;
            DataPoint *c = new DataPoint(input_size, output_size);
            c->label = label;

            for (int b = 0; b < OUTPUT_WIDTH; b++) {
                c->target->get(b) = (label == b) ? 1 : 0;
            }

            float value;
            for (int y = 0; y < INPUT_HEIGHT; y++) {
                for (int x = 0; x < INPUT_WIDTH; x++) {
                    linestream >> value;
                    c->data->get(x, y) = value / 255.0f;
                }
            }
            cases.push_back(c);
        }
        return cases;
    }
}
