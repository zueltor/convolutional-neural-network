#include "FloatArray.h"

#include <cmath>
using namespace CNN;

void FloatArray::print(std::ostream &out) const {
    for (int z = 0; z < size.depth; z++) {
        for (int y = 0; y < size.height; y++) {
            for (int x = 0; x < size.width; x++) {
                out << get(x, y, z) << " ";
            }

        }
    }
    out << std::endl;
}

void FloatArray::printMatrix(std::ostream &out) const {
    for (int z = 0; z < size.depth; z++) {
        for (int label = 0; label < size.height; label++) {
            for (int result = 0; result < size.width; result++) {
                out << int(get(result, label, z)) << "\t";
            }
            out<<std::endl;

        }
        out << std::endl;
    }

}

int FloatArray::maxPosition() {
    float max=-FLT_MAX;
    int index=-1;
    for (int i = 0; i < size.width * size.height * size.depth; i++) {
        if(max<values[i]){
            max=values[i];
            index=i;
        }
    }
    return index;
}

FloatArray::FloatArray(const FloatArray &t) {
    values = new float[t.size.width * t.size.height * t.size.depth];
    memcpy(this->values, t.values, t.size.width * t.size.height * t.size.depth * sizeof(float));
    this->size = t.size;
}

FloatArray *FloatArray::difference(FloatArray *output, FloatArray *target) {

    auto *result = new FloatArray(*output);
    for (int i = 0; i < target->size.width * target->size.height * target->size.depth; i++) {
        result->values[i] -= target->values[i];
    }
    return result;

}

float FloatArray::crossEntropy(FloatArray *output, FloatArray *target) {
    float error=0;
    for (int i = 0; i < target->size.width * target->size.height * target->size.depth; i++) {
        error-=target->values[i]*std::log2(output->values[i]);
    }
    return error;

}

float &FloatArray::get(int x, int y, int z) const {
    if (x < 0 || y < 0 || z < 0 || x >= size.width || y >= size.height || z >= size.depth) {
        throw std::out_of_range("Point is out of range");
    }
    return values[z * (size.width * size.height) + y * size.width + x];
}

float &FloatArray::get(int x, int y) const {
    if (x < 0 || y < 0 || x >= size.width || y >= size.height) {
        throw std::out_of_range("Point is out of range");
    }
    return values[y * size.width + x];
}

float &FloatArray::get(int x) const {
    if (x < 0 || x >= size.width) {
        throw std::out_of_range("Point is out of range");
    }
    return values[x];
}

FloatArray::~FloatArray() {
        delete[] values;
}
