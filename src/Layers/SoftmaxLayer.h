//
// Created by Damir on 28.05.2021.
//

#ifndef LAB3_SOFTMAXLAYER_H
#define LAB3_SOFTMAXLAYER_H


#include "Layer.h"

namespace CNN {

    class SoftmaxLayer : public Layer {

    public:

        explicit SoftmaxLayer(size_array in_size);

        void activate(FloatArray *in) override;

        void calculateInputGradients(FloatArray &grad_next_layer) override;

        ~SoftmaxLayer() override;

        void saveWeights(const std::string &filename) override;

        void loadWeights(std::istream &in) override;

    };

}


#endif //LAB3_SOFTMAXLAYER_H
