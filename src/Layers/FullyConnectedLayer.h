#ifndef LAB3_FULLYCONNECTEDLAYER_H
#define LAB3_FULLYCONNECTEDLAYER_H

#include <vector>
#include <cmath>
#include <cstring>
#include <fstream>
#include "Layer.h"

namespace CNN {

    class FullyConnectedLayer : public Layer {

    public:

        FloatArray *weights;
        std::vector<float> gradients;

        FullyConnectedLayer(size_array in_size, int width);

        int map(Point d);

        void activate(FloatArray *in) override;

        void updateWeights() override;

        void calculateInputGradients(FloatArray &grad_next_layer) override;

        ~FullyConnectedLayer() override;

        void saveWeights(const std::string &filename) override;

        void loadWeights(std::istream &in) override;

    };

}


#endif //LAB3_FULLYCONNECTEDLAYER_H
