#include "FullyConnectedLayer.h"

using namespace CNN;

FullyConnectedLayer::FullyConnectedLayer(size_array in_size, int width) {
    input_gradients = new FloatArray(in_size.width, in_size.height, in_size.depth);
    gradients = std::vector<float>(width);
    input = new FloatArray(in_size.width, in_size.height, in_size.depth);
    output = new FloatArray(width, 1, 1);
    weights = new FloatArray(in_size.width * in_size.height * in_size.depth, width, 1);
    for (int i = 0; i < width; i++) {
        for (int h = 0; h < in_size.width * in_size.height * in_size.depth; h++) {
            weights->get(h, i) = rand() / float(RAND_MAX) * 0.6 - 0.3;
        }
    }
}

int FullyConnectedLayer::map(Point d) {
    return d.z * (input->size.width * input->size.height) + d.y * (input->size.width) + d.x;
}

void FullyConnectedLayer::activate(FloatArray *in) {
    this->input = in;
    for (int n = 0; n < output->size.width; n++) {
        float value = 0;
        for (int i = 0; i < input->size.width; i++) {
            for (int j = 0; j < input->size.height; j++) {
                for (int z = 0; z < input->size.depth; z++) {
                    int m = map({i, j, z});
                    value += input->get(i, j, z) * weights->get(m, n);
                }
            }
        }
        output->get(n) = value;
    }
}

void FullyConnectedLayer::updateWeights() {
    for (int n = 0; n < output->size.width; n++) {
        float &grad = gradients[n];
        for (int i = 0; i < input->size.width; i++) {
            for (int j = 0; j < input->size.height; j++) {
                for (int z = 0; z < input->size.depth; z++) {
                    int m = map({i, j, z});
                    float &w = weights->get(m, n);
                    w = update_weight(w, grad, input->get(i, j, z));
                }
            }
        }
    }
}

void FullyConnectedLayer::calculateInputGradients(FloatArray &grad_next_layer) {
    memset(input_gradients->values, 0,
           input_gradients->size.width * input_gradients->size.height * input_gradients->size.depth *
           sizeof(float));
    for (int n = 0; n < output->size.width; n++) {
        float &grad = gradients[n];
        grad = grad_next_layer.get(n);
        for (int i = 0; i < input->size.width; i++) {
            for (int j = 0; j < input->size.height; j++) {
                for (int z = 0; z < input->size.depth; z++) {
                    int m = map({i, j, z});
                    input_gradients->get(i, j, z) += grad * weights->get(m, n);
                }
            }
        }
    }
}

FullyConnectedLayer::~FullyConnectedLayer() {
    delete input_gradients;
    delete input;
    delete output;
}

void FullyConnectedLayer::saveWeights(const std::string &filename) {
    std::ofstream file(filename, std::ofstream::app);
    weights->print(file);
}

void FullyConnectedLayer::loadWeights(std::istream &in) {
    weights->read(in);
}
