#ifndef LAB3_RELULAYER_H
#define LAB3_RELULAYER_H

#include "Layer.h"

namespace CNN {

    class ReLuLayer : public Layer {

    public:

        explicit ReLuLayer(size_array in_size);

        void activate(FloatArray *in) override;

        void calculateInputGradients(FloatArray &grad_next_layer) override;

        ~ReLuLayer() override;

        void saveWeights(const std::string &filename) override;

        void loadWeights(std::istream &in) override;

    };

}


#endif //LAB3_RELULAYER_H
