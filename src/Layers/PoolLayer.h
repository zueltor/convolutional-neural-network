//
// Created by Damir on 28.05.2021.
//

#ifndef LAB3_POOLLAYER_H
#define LAB3_POOLLAYER_H


#include "Layer.h"

namespace CNN {
    class PoolLayer : public Layer {
    public:
        int stride, patch_size;
        size_array input_size;

        PoolLayer(int stride, int patch, size_array in_size);

        Point mapToInput(Point out, int z);

        static int normalizeRange(float f, int max, bool lim_min);

        range_array mapToOutput(int x, int y);

        void activate(FloatArray *in) override;

        void calculateInputGradients(FloatArray &grad_next_layer) override;

        ~PoolLayer() override;

    };
}


#endif //LAB3_POOLLAYER_H
