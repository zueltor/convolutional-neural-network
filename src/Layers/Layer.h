#ifndef LAB3_LAYER_H
#define LAB3_LAYER_H

#include "../FloatArray.h"

namespace CNN {

    class Layer {
    public:
        FloatArray *input_gradients;
        FloatArray *input;
        FloatArray *output;

        virtual void activate(FloatArray *) = 0;

        virtual void calculateInputGradients(FloatArray &) = 0;

        virtual void saveWeights(const std::string &filename);

        virtual void loadWeights(std::istream &in);

        virtual ~Layer();

        virtual void updateWeights();

    };

}


#endif //LAB3_LAYER_H
