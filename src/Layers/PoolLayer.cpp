//
// Created by Damir on 28.05.2021.
//

#include "PoolLayer.h"

#include <cmath>

using namespace CNN;

PoolLayer::PoolLayer(int stride, int patch, size_array in_size)
        : stride(stride), patch_size(patch), input_size(in_size) {
    input_gradients = new FloatArray(in_size.width, in_size.height, in_size.depth);
    input = new FloatArray(in_size.width, in_size.height, in_size.depth);
    output = new FloatArray((in_size.width - patch) / stride + 1, (in_size.height - patch) / stride + 1, in_size.depth);
}

Point PoolLayer::mapToInput(Point out, int z) {
    out.x *= stride;
    out.y *= stride;
    out.z = z;
    return out;
}

int PoolLayer::normalizeRange(float f, int max, bool lim_min) {
    if (f <= 0) { return 0; }
    max -= 1;
    if (f >= max) { return max; }
    if (lim_min) {
        return std::ceil(f);
    } else {
        return std::floor(f);
    }
}

range_array PoolLayer::mapToOutput(int x, int y) {
    float a = x;
    float b = y;
    return {
            normalizeRange((a - patch_size + 1) / stride, output->size.width, true),
            normalizeRange((b - patch_size + 1) / stride, output->size.height, true),

            normalizeRange(a / stride, output->size.width, false),
            normalizeRange(b / stride, output->size.height, false)
    };
}

void PoolLayer::activate(FloatArray *in) {
    this->input = in;
    for (int z = 0; z < output->size.depth; z++) {
        for (int x = 0; x < output->size.width; x++) {
            for (int y = 0; y < output->size.height; y++) {
                Point mapped = mapToInput({x, y, 0}, 0);
                float max_value = -FLT_MAX;
                for (int i = 0; i < patch_size; i++) {
                    for (int j = 0; j < patch_size; j++) {
                        float value = input->get(mapped.x + i, mapped.y + j, z);
                        if (value > max_value) {
                            max_value = value;
                        }
                    }
                }
                output->get(x, y, z) = max_value;
            }
        }
    }
}

void PoolLayer::calculateInputGradients(FloatArray &grad_next_layer) {
    for (int y = 0; y < input_size.height; y++) {
        for (int x = 0; x < input_size.width; x++) {
            range_array rn = mapToOutput(x, y);
            for (int z = 0; z < input_size.depth; z++) {
                float sum_error = 0;
                for (int i = rn.min_x; i <= rn.max_x; i++) {
                    for (int j = rn.min_y; j <= rn.max_y; j++) {
                        int is_max = (input->get(x, y, z) == output->get(i, j, z)) ? 1 : 0;
                        sum_error += is_max * grad_next_layer.get(i, j, z);
                    }
                }
                input_gradients->get(x, y, z) = sum_error;
            }
        }
    }
}

PoolLayer::~PoolLayer() {
    delete input_gradients;
    delete input;
    delete output;
}
