#include "Layer.h"

using namespace CNN;

void Layer::saveWeights(const std::string &filename) {}

void Layer::loadWeights(std::istream &in) {}

void Layer::updateWeights() {}

Layer::~Layer() = default;
