#include "SoftmaxLayer.h"
#include <cmath>

using namespace CNN;

SoftmaxLayer::SoftmaxLayer(size_array in_size) {
    input_gradients = new FloatArray(in_size.width, in_size.height, in_size.depth);
    input = new FloatArray(in_size.width, in_size.height, in_size.depth);
    output = new FloatArray(in_size.width, in_size.height, in_size.depth);
}

void SoftmaxLayer::activate(FloatArray *in) {
    this->input = in;

    float sum = 0;
    for (int z = 0; z < in->size.depth; z++) {
        for (int y = 0; y < in->size.height; y++) {
            for (int x = 0; x < in->size.width; x++) {
                sum += std::exp(in->get(x, y, z));
            }
        }
    }

    for (int z = 0; z < in->size.depth; z++) {
        for (int y = 0; y < in->size.height; y++) {
            for (int x = 0; x < in->size.width; x++) {
                output->get(x, y, z) = std::exp(in->get(x, y, z)) / sum;
            }
        }

    }
}

void SoftmaxLayer::calculateInputGradients(FloatArray &grad_next_layer) {
    for (int x = 0; x < input->size.width; x++) {
        for (int y = 0; y < input->size.height; y++) {
            for (int z = 0; z < input->size.depth; z++) {
                input_gradients->get(x, y, z) = grad_next_layer.get(x, y,z);
            }
        }
    }
}

SoftmaxLayer::~SoftmaxLayer() {
    delete input_gradients;
    delete input;
    delete output;
}

void SoftmaxLayer::saveWeights(const std::string &filename) {
}

void SoftmaxLayer::loadWeights(std::istream &in) {
}
