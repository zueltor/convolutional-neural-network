//
// Created by Damir on 28.05.2021.
//

#ifndef LAB3_CONVOLUTIONALLAYER_H
#define LAB3_CONVOLUTIONALLAYER_H

#include <iostream>
#include <vector>
#include <fstream>
#include "../FloatArray.h"
#include "Layer.h"

namespace CNN {
    class ConvolutionalLayer : public Layer {

    public:
        std::vector<FloatArray *> filters;
        std::vector<FloatArray *> filter_gradients;
        int stride, kernel_size;

        ConvolutionalLayer(int stride, int kernel_size, int number_filters, size_array in_size);


        Point mapToInput(Point out, int z);

        static int normalizeRange(float f, int max, bool lim_min);

        range_array mapToOutput(int x, int y);

        void activate(FloatArray *in) override;

        void updateWeights() override;

        void calculateInputGradients(FloatArray &grad_next_layer) override;

        void resetGradients() const;

        ~ConvolutionalLayer() override;

        void saveWeights(const std::string &filename) override;

        void loadWeights(std::istream &in) override;


    };
}
#endif //LAB3_CONVOLUTIONALLAYER_H
