#include "ReLuLayer.h"

using namespace CNN;

ReLuLayer::ReLuLayer(size_array in_size) {
    input_gradients = new FloatArray(in_size.width, in_size.height, in_size.depth);
    input = new FloatArray(in_size.width, in_size.height, in_size.depth);
    output = new FloatArray(in_size.width, in_size.height, in_size.depth);
}

void ReLuLayer::activate(FloatArray *in) {
    this->input = in;
    for (int z = 0; z < input->size.depth; z++) {
        for (int x = 0; x < input->size.width; x++) {
            for (int y = 0; y < input->size.height; y++) {
                float value = input->get(x, y, z);
                if (value < 0)
                    value = 0;
                output->get(x, y, z) = value;

            }
        }

    }
}

void ReLuLayer::calculateInputGradients(FloatArray &grad_next_layer) {
    for (int x = 0; x < input->size.width; x++) {
        for (int y = 0; y < input->size.height; y++) {
            for (int z = 0; z < input->size.depth; z++) {
                input_gradients->get(x, y, z) = (input->get(x, y, z) < 0) ? 0 : grad_next_layer.get(x, y, z);
            }
        }
    }

}

ReLuLayer::~ReLuLayer() {
    delete input_gradients;
    delete input;
    delete output;
}

void ReLuLayer::saveWeights(const std::string &filename) {
}

void ReLuLayer::loadWeights(std::istream &in) {
}
