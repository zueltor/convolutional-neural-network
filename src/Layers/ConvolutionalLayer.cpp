#include "ConvolutionalLayer.h"

#include <cmath>

using namespace CNN;

ConvolutionalLayer::ConvolutionalLayer(int stride, int kernel_size, int number_filters, size_array in_size) {
    input_gradients = new FloatArray(in_size.width, in_size.height, in_size.depth);
    input = new FloatArray(in_size.width, in_size.height, in_size.depth);
    output = new FloatArray((in_size.width - kernel_size) / stride + 1,
                            (in_size.height - kernel_size) / stride + 1, number_filters);
    this->stride = stride;
    this->kernel_size = kernel_size;

    for (int a = 0; a < number_filters; a++) {
        auto *filter = new FloatArray(kernel_size, kernel_size, in_size.depth);
        for (int x = 0; x < kernel_size; x++) {
            for (int y = 0; y < kernel_size; y++) {
                for (int z = 0; z < in_size.depth; z++) {
                    float value = rand() / float(RAND_MAX) * 0.6 - 0.3;

                    filter->get(x, y, z) = value;
                }
            }
        }
        filters.push_back(filter);
    }

    for (int i = 0; i < number_filters; i++) {
        auto *filter_gradient = new FloatArray(kernel_size, kernel_size, in_size.depth);
        filter_gradients.push_back(filter_gradient);
    }

}

Point ConvolutionalLayer::mapToInput(Point out, int z) {
    out.x *= stride;
    out.y *= stride;
    out.z = z;
    return out;
}

int ConvolutionalLayer::normalizeRange(float f, int max, bool lim_min) {
    if (f <= 0) { return 0; }
    max -= 1;
    if (f >= max) { return max; }

    if (lim_min) {
        return std::ceil(f);
    } else {
        return std::floor(f);
    }
}

range_array ConvolutionalLayer::mapToOutput(int x, int y) {
    float a = x;
    float b = y;
    return {
            normalizeRange((a - kernel_size + 1) / stride, output->size.width, true),
            normalizeRange((b - kernel_size + 1) / stride, output->size.height, true),
            normalizeRange(a / stride, output->size.width, false),
            normalizeRange(b / stride, output->size.height, false)
    };
}

void ConvolutionalLayer::activate(FloatArray *in) {
    this->input = in;
    for (int filter = 0; filter < filters.size(); filter++) {
        FloatArray *filter_data = filters[filter];
        for (int y = 0; y < output->size.height; y++) {
            for (int x = 0; x < output->size.width; x++) {
                Point mapped = mapToInput({x, y, 0}, 0);
                float sum = 0;
                for (int i = 0; i < kernel_size; i++) {
                    for (int j = 0; j < kernel_size; j++) {
                        for (int z = 0; z < input->size.depth; z++) {
                            float f = filter_data->get(i, j, z);
                            float v = input->get(mapped.x + i, mapped.y + j, z);
                            sum += f * v;
                        }
                    }
                }
                output->get(x, y, filter) = sum;
            }
        }
    }
}

void ConvolutionalLayer::updateWeights() {
    for (int k = 0; k < filters.size(); k++) {
        for (int y = 0; y < kernel_size; y++) {
            for (int x = 0; x < kernel_size; x++) {
                for (int z = 0; z < input->size.depth; z++) {
                    FloatArray *filter = filters[k];
                    float &w = filter->get(x, y, z);
                    FloatArray *tensor_gradient = filter_gradients[k];
                    float grad = tensor_gradient->get(x, y, z);
                    w = update_weight(w, grad);
                }
            }
        }
    }
}

void ConvolutionalLayer::calculateInputGradients(FloatArray &grad_next_layer) {
    resetGradients();
    for (int x = 0; x < input->size.width; x++) {
        for (int y = 0; y < input->size.height; y++) {
            range_array rn = mapToOutput(x, y);
            for (int z = 0; z < input->size.depth; z++) {
                float sum_error = 0;
                for (int i = rn.min_x; i <= rn.max_x; i++) {
                    int minx = i * stride;
                    for (int j = rn.min_y; j <= rn.max_y; j++) {
                        int miny = j * stride;
                        for (int k = 0; k < filters.size(); k++) {
                            int w_applied = filters[k]->get(x - minx, y - miny, z);
                            sum_error += w_applied * grad_next_layer.get(i, j, k);
                            float value = input->get(x, y, z) * grad_next_layer.get(i, j, k);
                            filter_gradients[k]->get(x - minx, y - miny, z) += value;
                        }
                    }
                }
                input_gradients->get(x, y, z) = sum_error;
            }
        }
    }
}

void ConvolutionalLayer::resetGradients() const {
    for (auto filter_gradient : filter_gradients) {
        for (int x = 0; x < kernel_size; x++) {
            for (int y = 0; y < kernel_size; y++) {
                for (int z = 0; z < input->size.depth; z++) {
                    filter_gradient->get(x, y, z) = 0;
                }
            }
        }
    }
}

ConvolutionalLayer::~ConvolutionalLayer() {
    for (auto &filter : filters) {
        delete filter;
    }
    for (auto &filter_gradient : filter_gradients) {
        delete filter_gradient;
    }
    delete input_gradients;
    delete input;
    delete output;
}

void ConvolutionalLayer::saveWeights(const std::string &filename) {
    std::ofstream file(filename, std::ofstream::app);
    for (auto &filter:filters) {
        filter->print(file);
    }
}

void ConvolutionalLayer::loadWeights(std::istream &in) {
    for (auto &filter:filters) {
        filter->read(in);
    }
}
