//
// Created by Damir on 28.05.2021.
//

#include "ConfusionMatrix.h"
using namespace CNN;
ConfusionMatrix::ConfusionMatrix(CNN::FloatArray *matrix) : matrix(matrix),
                                                                 precision(std::vector<float>(matrix->size.width)),
                                                                 recall(std::vector<float>(matrix->size.width)),
                                                                 fp(std::vector<int>(matrix->size.width)),
                                                                 fn(std::vector<int>(matrix->size.width)),
                                                                 tp(std::vector<int>(matrix->size.width)),
                                                                 auc(std::vector<float>(matrix->size.width)),
                                                                 f_score(std::vector<float>(matrix->size.width)),
                                                                 positive(std::vector<int>(matrix->size.width)),
                                                                 negative(std::vector<int>(matrix->size.width)),
                                                                 fpr(std::vector<std::vector<float>>(matrix->size.width)),
                                                                 tpr(std::vector<std::vector<float>>(matrix->size.width)){
    size = matrix->size;

}

void ConfusionMatrix::computeMetrics() {
    int n = 0;
    int correct = 0;
    for (int i = 0; i < tp.size(); i++) {
        tp[i] = matrix->get(i, i);
        correct += tp[i];
    }

    for (int target = 0; target < size.height; target++) {
        for (int output = 0; output < size.width; output++) {
            fp[output] += matrix->get(output, target);
            fn[target] += matrix->get(output, target);
            n += (int) matrix->get(output, target);
        }
    }

    accuracy = float(correct) / float(n);
    for (int i = 0; i < tp.size(); i++) {
        fp[i] -= tp[i];
        fn[i] -= tp[i];
        precision[i] = float(tp[i]) / float(tp[i] + fp[i]);
        recall[i] = float(tp[i]) / float(tp[i] + fn[i]);
        f_score[i] = 2 * precision[i] * recall[i] / (precision[i] + recall[i]);
    }
}

void ConfusionMatrix::saveROCCurves(std::vector<DataPoint *> &cases) {
    std::vector<std::pair<int, FloatArray *>> results;
    int count = 0;
    for (auto &input_case:cases) {
        positive[input_case->label]++;
        count++;
        results.emplace_back(input_case->label, input_case->output);
    }

    for (int i = 0; i < positive.size(); i++) {
        fp[i] = 0;
        tp[i] = 0;
        negative[i] = cases.size() - positive[i];
        std::cout << "Positive " << i << ": " << positive[i]
                  << " Negative " << i << ": " << negative[i] << std::endl;
    }
    for (int label = 0; label < size.width; label++) {
        std::sort(results.begin(), results.end(),
                  [label](const std::pair<int, FloatArray *> &a,
                          const std::pair<int, FloatArray *> &b) -> bool {
                      return a.second->get(label) > b.second->get(label);
                  });
        float fpr_prev = 0;
        float tpr_prev = 0;
        fpr[label].push_back(fpr_prev);
        tpr[label].push_back(tpr_prev);

        int prev_i = -1;
        for (int i = 0; i < results.size(); i++) {
            if (results[i].first == label) {
                tp[label]++;
            } else {
                fp[label]++;
                float fpr_next = float(fp[label]) / negative[label];
                float tpr_next = float(tp[label]) / positive[label];
                auc[label] += (tpr_next + tpr_prev) * (fpr_next - fpr_prev) / 2;
                if (i - prev_i >= 10) {
                    fpr[label].push_back(fpr_next);
                    tpr[label].push_back(tpr_next);
                    prev_i = i;
                }
                fpr_prev = fpr_next;
                tpr_prev = tpr_next;
            }

        }
        std::ofstream file(std::to_string(label) + ".txt");
        for (int i = 0; i < fpr[label].size(); i++) {
            file << fpr[label][i] << ", " << tpr[label][i] << std::endl;
        }
        file.close();
        std::ofstream auc_file("auc.txt");
        for (int i = 0; i < auc.size(); i++) {
            auc_file << "AUC for " << i << ": " << auc[i] << std::endl;
        }
    }
}

void ConfusionMatrix::printMetrics(std::ostream &out) {
    out << "Accuracy: " << accuracy * 100 << "%" << std::endl;
    auto old_precision = out.precision(4);
    out << "Precision: " << std::endl;
    for (float p : precision) {
        out << p * 100 << "%\t";
    }
    out << std::endl;

    out << "Recall: " << std::endl;
    for (float r : recall) {
        out << r * 100 << "%\t";
    }
    out << std::endl;

    out << "F-score: " << std::endl;
    for (float f : f_score) {
        out << f * 100 << "%\t";
    }
    out << std::endl;
    out << "Confusion Matrix:" << std::endl;
    matrix->printMatrix(out);
    out.precision(old_precision);
}
