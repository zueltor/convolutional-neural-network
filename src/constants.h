#ifndef LAB3_CONSTANTS_H
#define LAB3_CONSTANTS_H

namespace CNN {
    constexpr auto INPUT_WIDTH = 28;
    constexpr auto INPUT_HEIGHT = 28;
    constexpr auto INPUT_DEPTH = 1;
    constexpr auto OUTPUT_WIDTH = 10;
    constexpr auto OUTPUT_HEIGHT = 1;
    constexpr auto OUTPUT_DEPTH = 1;
    constexpr auto LEARNING_RATE = 0.02;
    constexpr auto GAMMA = 0.6;
}

#endif //LAB3_CONSTANTS_H
